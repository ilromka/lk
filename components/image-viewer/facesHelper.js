export default {
    getBestSimilarFace (face) {
        return this._getBestSimilarFace(face);
    },
    getGalleries (face) {
        let result = [];
        if (this._isSimilarFacesExist(face)) {
            face.similarFaces.forEach(item => {
                if (!result.some(gallery => gallery.origin === item.file.it.origin)) {
                    result.push({
                        origin: item.file.it.origin,
                        name: this._getGalleryName(item.file.it.origin),
                        selected: false,
                        count: this._getGalleryCount(face, item.file.it.origin),
                    });
                }
            });
        }
        if (!!result.length) {
            result.sort((a,b) => (a.count > b.count) ? -1 : ((b.count > a.count) ? 1 : 0));
            let bestGalleryOrigin = this._getBestSimilarFace(face).file.it.origin;
            let bestGallery = JSON.parse(JSON.stringify(result.find(item => item.origin === bestGalleryOrigin)));
            result = result.filter(item => item.origin !== bestGalleryOrigin);
            result.unshift(bestGallery);
            result[0].selected = true;
        }
        return result;
    },
    getGallerySimilarFaces (face, origin) {
        let result = [];
        if (this._isSimilarFacesExist(face)) {
            face.similarFaces.forEach(item => {
                if (item.file.it.origin === origin) {
                    item.selected = false;
                    result.push(item);
                }
            });
        }
        if (!!result.length) {
            result.sort((a,b) => (a.score > b.score) ? -1 : ((b.score > a.score) ? 1 : 0));
            if (result.length > 5) {
                result.length = 5;
            }
            result[0].selected = true;
        }
        return result;
    },
    _isSimilarFacesExist (face) {
        return face.hasOwnProperty('similarFaces') && Array.isArray(face.similarFaces) && !!face.similarFaces.length;
    },
    _getBestSimilarFace (face) {
        let result = null;
        if (this._isSimilarFacesExist(face)) {
            let score = 0;
            face.similarFaces.forEach(item => {
                if (Number(item.score) >= score && item.hasOwnProperty('name') && !!item.name.length) {
                    score = item.score;
                    result = item;
                }
            });
        }
        return result;
    },
    _getGalleryName (origin) {
        const names = {
            'msm': 'Метапоиск',
            'iqp_object': 'Аналитика-Объекты',
            'iqp_article': 'Аналитика-Документы',
        };
        let result = names[origin];
        if (!result) {
            result = 'Другие';
        }
        return result;
    },
    _getGalleryCount (face, origin) {
        let result = 0;
        if (this._isSimilarFacesExist(face)) {
            face.similarFaces.forEach(item => {
                if (item.file.it.origin === origin) {
                    result ++;
                }
            });
        }
        return result;
    },
};
