import Router from 'vue-router';
import Lk from './../views/Lk';

export default new Router({
    routes: [{
        name: 'Lk',
        path: '',
        component: Lk
    }]
});