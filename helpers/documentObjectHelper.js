export default {
    getAvatar (object) {
        let result = null;
        if (object && object.attributes && object.attributes.AvatarPicture && !!object.attributes.AvatarPicture.length) {
            result = `data:image/png;base64,${object.attributes.AvatarPicture.replace(/_/gmi, '/').replace(/\$/gmi, '+')}`;
        }
        return result;
    },
    getOneString (object) {
        return (object && object.oneString) || null;
    },
    getProperties (object) {
        let result = [];
        if (object && object.attributes) {
            Object.keys(object.attributes).forEach(key => {
                if (key !== 'AvatarPicture') {
                    result.push(key);
                }
            });
        }
        return result;
    },
};