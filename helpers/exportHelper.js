export default {
    download (service, appRoot, ids) {
        return new Promise((resolve, reject) => {
            service({documents: ids}).then(resp => {
                if (resp) {
                    let url = `${appRoot}/action/new/attachment?contentType=application/force-download&attachId=${resp.data.id}`;
                    let link = document.createElement('a');
                    link.href = url;
                    link.download = resp.data.filename;
                    let e = document.createEvent('MouseEvents');
                    e.initEvent('click', true, true);
                    link.dispatchEvent(e);
                    resolve();
                } else {
                    reject();
                }
            });
        });
    },
    downloadFootnotes (service, appRoot, ids) {
        return new Promise((resolve, reject) => {
            service({footnotes: ids}).then(resp => {
                if (resp) {
                    let url = `${appRoot}/action/new/attachment?contentType=application/force-download&attachId=${resp.data.id}`;
                    let link = document.createElement('a');
                    link.href = url;
                    link.download = resp.data.filename;
                    let e = document.createEvent('MouseEvents');
                    e.initEvent('click', true, true);
                    link.dispatchEvent(e);
                    resolve();
                } else {
                    reject();
                }
            });
        });
    },
};