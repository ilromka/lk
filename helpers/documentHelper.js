import moment from 'moment';

moment.locale('ru');

const REGEXP = {
    TAGS: /<[^>]*>/gmi,
    TAGS_EXCLUDING_SPAN: /<(?!\/?span)[^>]*>/gmi,
    TAGS_EXCLUDING_SPAN_AND_BR: /<(?!\/?br)(?!\/?span)[^>]*>/gmi,
    BR: /<br\s*\/*>/gmi,
    SEVERAL_BR: /(<br\s*\/*>\S*\s*){3,}/gmi,
    QUOT: /&quot;/gmi,
    IMAGES: /iqimage[a-z0-9]*/gmi,
    NEWLINES: /\r?\n|\r/gmi,
    SEVERAL_SPACES: /\s+/gmi,
};

export default {
    prepareTitle: (document, translated = false) => {
        let result = 'Документ без заголовка';
        let text = null;
        if (translated && document.attributes.TITLE_TRANSLATION) {
            text = document.attributes.TITLE_TRANSLATION;
        } else {
            if (document.title) {
                text = document.title;
            }
        }
        if (text) {
            result = text.replace(REGEXP.IMAGES, '').replace(REGEXP.TAGS, '').replace(REGEXP.QUOT, '"').trim();
        }
        return result;
    },
    prepareText: (document, translated = false) => {
        let result = 'Документ без текста';
        let text = null;
        if (translated && document.attributes.TEXT_TRANSLATION) {
            text = document.attributes.TEXT_TRANSLATION;
        } else {
            if (document.text) {
                text = document.text;
            }
        }
        if (text) {
            result = text.replace(REGEXP.IMAGES, '').replace(REGEXP.TAGS_EXCLUDING_SPAN, '').replace(REGEXP.QUOT, '"').trim();
        }
        return result;
    },
    prepareFullText (text, appRoot) {
        let inlineImages = text.replace(REGEXP.TAGS, '#').match(REGEXP.IMAGES) || [];
        inlineImages = inlineImages.map(item => {
            item = item.replace('iqimage', '').trim();
            return item;
        });
        let preparedText = text.replace(REGEXP.NEWLINES, '').replace(REGEXP.SEVERAL_SPACES, ' ').replace(REGEXP.SEVERAL_BR, '<br><br>').replace(REGEXP.QUOT, '"').trim();
        inlineImages.forEach(item => {
            preparedText = preparedText.replace(`iqimage${item}`, `<div><img style="max-width: 300px; max-height: 300px" src="${appRoot}/iqplatform/attachment?attachId=${item}"></div>`);
        });
        return preparedText;
    },
    getImages (document) {
        let result = null;
        if (document.attributes && document.attributes.FENCE_ATTACHIDS) {
            if (
                Array.isArray(document.attributes.FENCE_ATTACHIDS) &&
                document.attributes.FENCE_ATTACHIDS.length
            ) {
                result = document.attributes.FENCE_ATTACHIDS;
            }
        }
        // картинки из текста не берем
        /*if (!result) {
            let imageTags = document.text.match(REGEXP.IMAGES);
            if (imageTags && imageTags.length) {
                result = imageTags.map(tag => tag.replace('iqimage', ''));
            }
        }*/
        return result;
    },
    getDate (document) {
        return moment(document.docDate).format('DD MMMM YYYY HH:mm');
    },
    getSource (document) {
        let result = null;
        if (document.agency && document.agency.id && document.agency.name && document.agency.name.length) {
            result = document.agency;
        }
        return result;
    },
    getAuthors (document) {
        let result = [];
        if (document.hasOwnProperty('experts') && Array.isArray(document.experts) && !!document.experts.length) {
            result = document.experts;
        }
        return result;
    },
    prepareFootnoteText (text) {
        return text.replace(REGEXP.NEWLINES, '').replace(REGEXP.SEVERAL_SPACES, ' ').replace(REGEXP.SEVERAL_BR, '<br><br>').replace(REGEXP.QUOT, '"').replace(REGEXP.QUOT, '"').replace(REGEXP.BR, '\n').replace(REGEXP.TAGS, '').trim();
    },
    htmlDecode (text) {
        let e = document.createElement('textarea');
        e.innerHTML = text;
        return e.childNodes.length === 0 ? '' : e.childNodes[0].nodeValue;
    },
};