import Vue from '@/libs/vue.js';

const EventBus = new Vue();

const EVENTS = {
    SEARCH: 'SEARCH',
    UNSELECT_FEEDS: 'UNSELECT_FEEDS',
    UNSELECT_FAVORITES: 'UNSELECT_FAVORITES',
    UNSELECT_GENERAL_FAVORITES: 'UNSELECT_GENERAL_FAVORITES',
    UNSELECT_RECOMMENDATIONS: 'UNSELECT_RECOMMENDATIONS',
    SHOW_FAST_FEED_FORM: 'SHOW_FAST_FEED_FORM',
    DOCUMENT_TO_FAVORITE: 'DOCUMENT_TO_FAVORITE',
    DOCUMENT_FROM_FAVORITE: 'DOCUMENT_FROM_FAVORITE',
    SHOW_NOTIFICATIONS_BUTTON: 'SHOW_NOTIFICATIONS_BUTTON',
    FORCE_SHOW_NOTIFICATIONS: 'FORCE_SHOW_NOTIFICATIONS',
    LOADING: 'LOADING',
    CREATE_FEED_BY_OBJECT: 'CREATE_FEED_BY_OBJECT',
    ADD_OBJECT_TO_FEED: 'ADD_OBJECT_TO_FEED',
    WHEEL_OUTSIDE: 'WHEEL_OUTSIDE',
};

const TABS = {
    DOCUMENTS: 'DOCUMENTS',
    CHANNELS: 'CHANNELS',
    CHANNEL: 'CHANNEL',
    FEED_FORM: 'FEED_FORM',
};

const REQUEST = {
    COUNT: 20,
};

const FEED_TYPES = {
    DOCUMENTS: 'DOCUMENTS',
    IMAGES: 'IMAGES',
};

export default {
    EventBus,
    EVENTS,
    TABS,
    REQUEST,
    FEED_TYPES,
};