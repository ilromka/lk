__webpack_public_path__ = window.iqmen.initData.contextPath + '/static/';

import Vue from '@/libs/vue.js';
import router from './router';
import store from './store';
import i18n from '@/commons/i18n';
import i18n_ru from './../locale/ru';
import Modal from '@/commons/components/Modal';
import LkApp from './LkApp';
import '@/libs/fontawesome/styles.scss';

i18n.mergeLocaleMessage('ru', i18n_ru);
Vue.use(Modal, {dynamic: true, injectModalsContainer: true});

store.dispatch('init').then(() => {
    new Vue({
        i18n,
        router,
        store,
        render: h => h(LkApp)
    }).$mount('#app');
});

export default store;
