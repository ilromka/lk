import services from '@/commons/services';

const state = {
    list: [],
    editing: null,
    preFill: null,
    selected: [],
    reCategorization: [],
};

const mutations = {
    feeds (state, feeds) {
        state.list = feeds;
    },
    addFeed (state, feed) {
        state.list.unshift(feed);
        if (!state.reCategorization.includes(feed.id)) {
            state.reCategorization.push(feed.id);
        }
    },
    saveFeed (state, feed) {
        state.list = state.list.map(item => {
            if (Number(item.id) === Number(feed.id)) {
                item = feed;
            }
            return item;
        });
        if (!state.reCategorization.includes(feed.id)) {
            state.reCategorization.push(feed.id);
        }
    },
    removeFeed (state, id) {
        state.list = state.list.filter(item => Number(item.id) !== Number(id));
        if (state.selected.includes(id.toString())) {
            state.selected = state.selected.filter(item => Number(item) !== Number(id));
        }
        state.reCategorization = state.reCategorization.filter(item => item !== id);
    },
    editingFeed (state, payload) {
        state.editing = payload;
    },
    preFillFeed (state, payload) {
        state.preFill = payload;
    },
    selectedFeeds (state, feeds) {
        state.selected = feeds;
    },
    fromReCategorization (state, id) {
        state.reCategorization = state.reCategorization.filter(item => item !== id);
    },
    appendFeed (state, feed) {
        state.list.unshift(feed);
        if (!state.reCategorization.includes(feed.id)) {
            state.reCategorization.push(feed.id);
        }
    },
};

const actions = {
    feeds (context, userId) {
        return new Promise((resolve, reject) => {
            services.subsystem_personalPage_feed.list({userId}).then(
                resp => {
                    context.commit('feeds', resp.data.list);
                    resolve();
                },
                () => {
                    reject();
                },
            );
        });
    },
    addFeed (context, data) {
        return new Promise((resolve, reject) => {
            services.subsystem_personalPage_feed.save(data).prepare().then(
                resp => {
                    context.commit('addFeed', resp.data);
                    resolve();
                },
                error => {
                    reject(error);
                },
            );
        });
    },
    saveFeed (context, data) {
        return new Promise((resolve, reject) => {
            services.subsystem_personalPage_feed.save(data).prepare().then(
                resp => {
                    context.commit('saveFeed', resp.data);
                    resolve();
                },
                error => {
                    reject(error);
                },
            );
        });
    },
    removeFeed (context, id) {
        let feed = context.state.list.find(item => Number(item.id) === Number(id));
        return new Promise((resolve, reject) => {
            services.subsystem_personalPage_feed.delete(feed).prepare().then(
                () => {
                    context.commit('removeFeed', id);
                    resolve();
                },
                () => {
                    reject();
                },
            );
        });
    },
    editingFeed (context, value) {
        context.commit('editingFeed', value);
    },
    preFillFeed (context, value) {
        context.commit('preFillFeed', value);
    },
    selectFeeds (context, ids) {
        context.commit('selectedFeeds', ids);
    },
    unselectAllFeeds (context) {
        context.commit('selectedFeeds', []);
    },
    feedFromReCategorization (context, id) {
        context.commit('fromReCategorization', id);
    },
    appendFeed (context, feed) {
        context.commit('appendFeed', feed);
    },
};

export default {
    state,
    actions,
    mutations
}
