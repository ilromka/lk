import CONST from '../const';

const state = {
    value: CONST.TABS.CHANNELS,
    prev: CONST.TABS.CHANNELS,
};

const mutations = {
    tab (state, payload) {
        if (state.value === CONST.TABS.DOCUMENTS || state.value === CONST.TABS.CHANNELS) {
            state.prev = state.value;
        }
        state.value = payload;
    },
};

const actions = {
    setTab (context, tab) {
        context.commit('tab', tab);
    }
};

export default {
    state,
    actions,
    mutations
}
