import services from '@/commons/services';

const state = {
    list: [],
    selected: [],
};

const mutations = {
    favorites (state, payload) {
        state.list = payload.map(item => {
            item.edit = false;
            return item;
        });
    },
    selectedFavorites (state, payload) {
        state.selected = payload;
    },
    addFavorite (state, payload) {
        state.list.unshift({...payload, edit: false});
    },
    saveFavorite (state, payload) {
        state.list = state.list.map(item => {
            if (Number(item.id) === Number(payload.id)) {
                item = payload;
            }
            return item;
        });
    },
    removeFavorite (state, payload) {
        state.list = state.list.filter(item => Number(item.id) !== Number(payload));
    },
};

const actions = {
    favorites (context, id) {
        return new Promise((resolve, reject) => {
            services.subsystem_personalPage_favorite.list({
                userId: id,
            }).then(
                resp => {
                    context.commit('favorites', resp.data);
                    resolve();
                },
                () => {
                    reject();
                },
            );
        });
    },
    selectFavorites (context, ids) {
        context.commit('selectedFavorites', ids);
    },
    addFavorite (context, data) {
        return new Promise((resolve, reject) => {
            services.subsystem_personalPage_favorite.save(data).prepare().then(
                resp => {
                    data.id = resp.data.id;
                    context.commit('addFavorite', data);
                    resolve();
                },
                () => {
                    reject();
                },
            );
        });
    },
    saveFavorite (context, data) {
        return new Promise((resolve, reject) => {
            services.subsystem_personalPage_favorite.save(data).prepare().then(
                () => {
                    context.commit('saveFavorite', {...data, edit: false});
                    resolve();
                },
                () => {
                    reject();
                },
            );
        });
    },
    editFavorite (context, id) {
        let favorite = context.state.list.find(item => Number(item.id) === Number(id));
        if (favorite) {
            favorite.edit = true;
            context.commit('saveFavorite', favorite);
        }
    },
    uneditFavorite (context, id) {
        let favorite = context.state.list.find(item => Number(item.id) === Number(id));
        if (favorite) {
            favorite.edit = false;
            context.commit('saveFavorite', favorite);
        }
    },
    removeFavorite (context, id) {
        return new Promise((resolve, reject) => {
            services.subsystem_personalPage_favorite.delete({favId: id}).prepare().then(
                () => {
                    context.commit('removeFavorite', id);
                    resolve();
                },
                () => {
                    reject();
                },
            );
        });
    },
};

export default {
    state,
    actions,
    mutations
}
