import services from '@/commons/services';

const state = {
    list: [],
    selected: [],
};

const mutations = {
    recommendations (state, payload) {
        state.list = payload;
    },
    selectRecommendation (state, ids) {
        state.selected = ids;
    },
    removeRecommendation (state, id) {
        state.list = state.list.filter(item => Number(item.id) !== Number(id));
    },
    unselectRecommendation (state, id) {
        state.selected = state.selected.filter(recommendationId => Number(recommendationId) !== Number(id));
    },
};

const actions = {
    recommendations (context, userId) {
        return new Promise((resolve, reject) => {
            services.subsystem_personalPage_recommendation.list({targetId: userId}).then(
                resp => {
                    context.commit('recommendations', resp.data.list);
                    resolve();
                },
                () => {
                    reject();
                },
            );
        });
    },
    selectRecommendation (context, id) {
        context.commit('selectRecommendation', id);
    },
    declineRecommendation (context, data) {
        return new Promise((resolve, reject) => {
            services.subsystem_personalPage_recommendation.decline(data.id, data.userId).prepare().then(
                () => {
                    context.commit('unselectRecommendation', data.id);
                    context.commit('removeRecommendation', data.id);
                    resolve();
                },
                () => {
                    reject();
                },
            );
        });
    },
    acceptRecommendation (context, data) {
        return new Promise((resolve, reject) => {
            services.subsystem_personalPage_recommendation.accept(data.id, data.userId).prepare().then(
                resp => {
                    context.commit('unselectRecommendation', data.id);
                    context.commit('removeRecommendation', data.id);
                    resolve(resp.data);
                },
                () => {
                    reject();
                },
            );
        });
    },
};

export default {
    state,
    actions,
    mutations
}