import services from '@/commons/services';

const state = {
    list: [],
};

const mutations = {
    languages (state, payload) {
        state.list = payload;
    },
};

const actions = {
    languages (context) {
        return new Promise((resolve, reject) => {
            services.subsystem_personalPage_feed.languages().then(
                resp => {
                    let languages = resp.data.map(item => {
                        return {
                            id: item.code,
                            name: item.langName,
                            alias: item.langId,
                        };
                    });
                    let langRu = languages.find(item => item.alias === 'RU');
                    languages = languages.filter(item => item.alias !== 'RU');
                    if (langRu) {
                        languages.unshift(langRu);
                    }
                    context.commit('languages', languages);
                    resolve();
                },
                () => {
                    reject();
                },
            );
        });
    },
};

export default {
    state,
    actions,
    mutations
}
