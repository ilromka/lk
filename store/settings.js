import services from '@/commons/services';

const state = {
    objects: [],
};

const mutations = {
    objects (state, payload) {
        state.objects = payload;
    },
};

const actions = {
    settings (context) {
        return new Promise((resolve, reject) => {
            services.subsystem_personalPage_settings.get().then(
                settingsResp => {
                    if (settingsResp.hasOwnProperty('data')) {
                        if (settingsResp.data !== null && settingsResp.data.hasOwnProperty('objectClasses') && Array.isArray(settingsResp.data.objectClasses) && !!settingsResp.data.objectClasses.length) {
                            services.innerSchema.list({ids: settingsResp.data.objectClasses}).then(
                                innerSchemaResp => {
                                    context.commit('objects', innerSchemaResp.data);
                                    resolve();
                                },
                                () => {
                                    reject();
                                },
                            );
                        } else {
                            resolve();
                        }
                    } else {
                        reject();
                    }
                },
                () => {
                    reject();
                },
            );
        });
    },
};

export default {
    state,
    actions,
    mutations
}
