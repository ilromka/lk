const state = {
    value: '',
};

const mutations = {
    searchString (state, payload) {
        state.value = payload;
    },
};

const actions = {
    setSearchString (context, string) {
        context.commit('searchString', string);
    }
};

export default {
    state,
    actions,
    mutations
}
