import services from '@/commons/services';
import CONST from '../const';

const cacheObject = {
    filled: false,
    loading: false,
    offset: 0,
    list: [],
};

const state = {
    initialized: false,
    offset: 0,
    total: 0,
    list: [],
    lastRequest: {},
    cache: cacheObject,
};

const mutations = {
    documentsOffset (state, offset) {
        state.offset = offset;
    },
    documentsTotal (state, total) {
        state.total = total;
    },
    documents (state, documents) {
        state.initialized = true;
        state.list = documents;
    },
    documentsAppend (state, documents) {
        state.list = [...state.list, ...documents];
    },
    documentsDelete (state, id) {
        state.list = state.list.filter(item => Number(item.id) !== Number(id));
    },
    lastRequest (state, lastRequest) {
        state.lastRequest = lastRequest;
    },
    updateDocument (state, document) {
        state.list = state.list.map(item => {
            if (Number(item.id) === Number(document.id)) {
                item = document;
            }
            return item;
        });
    },
    documentsCache (state, {offset, list}) {
        state.cache = {
            filled: true,
            loading: false,
            offset,
            list,
        };
    },
    documentsCacheLoading (state, value) {
        state.cache.loading = value;
    },
    documentsCacheClear (state) {
        state.cache = cacheObject;
    }
};

const actions = {
    setDocumentsOffset (context, value) {
        context.commit('documentsOffset', value);
    },
    documents (context, request) {
        context.commit('documentsOffset', 0);
        return new Promise((resolve, reject) => {
            CONST.EventBus.$emit(CONST.EVENTS.LOADING, true);
            services.subsystem_personalPage_feed.documents({
                offset: context.state.offset,
                count: CONST.REQUEST.COUNT,
                feedIds: request.feedIds,
                textPredicate: request.textPredicate,
            }).then(
                resp => {
                    CONST.EventBus.$emit(CONST.EVENTS.LOADING, false);
                    if (resp.data) {
                        if (resp.data.documents) {
                            context.commit('documents', resp.data.documents);
                            context.commit('documentsTotal', resp.data.info.total);
                        } else {
                            context.commit('documents', []);
                            context.commit('documentsTotal', 0);
                        }
                        context.commit('documentsCacheClear');
                        context.dispatch('_preloadAppendingDocuments', request);
                        resolve();
                    } else {
                        reject();
                    }
                },
                () => {
                    CONST.EventBus.$emit(CONST.EVENTS.LOADING, false);
                    reject();
                },
            );
        });
    },
    appendDocuments (context, request) {
        context.commit('documentsOffset', context.state.offset + CONST.REQUEST.COUNT);
        if (context.state.cache.filled && context.state.cache.offset === context.state.offset) {
            context.commit('documentsAppend', context.state.cache.list);
            context.dispatch('_preloadAppendingDocuments', request);
            return new Promise(resolve => resolve());
        } else {
            return new Promise((resolve, reject) => {
                services.subsystem_personalPage_feed.documents({
                    offset: context.state.offset,
                    count: CONST.REQUEST.COUNT,
                    feedIds: request.feedIds,
                    textPredicate: request.textPredicate,
                }).then(
                    resp => {
                        context.dispatch('_preloadAppendingDocuments', request);
                        if (resp.data && resp.data.documents) {
                            context.commit('documentsAppend', resp.data.documents);
                            resolve();
                        } else {
                            reject();
                        }
                    },
                    () => {
                        context.commit('documentsOffset', context.state.offset - CONST.REQUEST.COUNT);
                        reject();
                    },
                );
            });
        }

    },
    _preloadAppendingDocuments (context, request) {
        if (context.state.list.length < context.state.total) {
            let offset = context.state.offset + CONST.REQUEST.COUNT;
            context.commit('documentsCacheLoading', true);
            services.subsystem_personalPage_feed.documents({
                offset,
                count: CONST.REQUEST.COUNT,
                feedIds: request.feedIds,
                textPredicate: request.textPredicate,
            }).then(
                resp => {
                    context.commit('documentsCacheLoading', false);
                    if (resp.data && resp.data.documents) {
                        context.commit('documentsCache', {
                            offset,
                            list: resp.data.documents,
                        });
                    }
                },
                () => {
                    context.commit('documentsCacheLoading', false);
                },
            );
        }
    },
    favoriteDocuments (context, request) {
        context.commit('documentsOffset', 0);
        return new Promise((resolve, reject) => {
            services.subsystem_personalPage_favorite.documents({
                favId: request.favId,
            }).prepare().then(
                resp => {
                    if (resp.data) {
                        if (resp.data.documents) {
                            context.commit('documents', resp.data.documents);
                            context.commit('documentsTotal', resp.data.info.total);
                        } else {
                            context.commit('documents', []);
                            context.commit('documentsTotal', 0);
                        }
                        resolve();
                    } else {
                        reject();
                    }
                },
                () => {
                    reject();
                },
            );
        });
    },
    generalFavoriteDocuments (context, request) {
        context.commit('documentsOffset', 0);
        return new Promise((resolve, reject) => {
            services.subsystem_personalPage_generalFavorite.footnotes({
                generalFavId: request.generalFavId,
                content: request.content,
            }).prepare().then(
                resp => {
                    if (resp.data) {
                        if (resp.data) {
                            context.commit('documents', resp.data);
                            context.commit('documentsTotal', resp.data.length);
                        } else {
                            context.commit('documents', []);
                            context.commit('documentsTotal', 0);
                        }
                        resolve();
                    } else {
                        reject();
                    }
                },
                () => {
                    reject();
                },
            );
        });
    },
    setLastRequest (context, value) {
        context.commit('lastRequest', value);
    },
    documentToFavorite (context, data) {
        return new Promise((resolve, reject) => {
            services.subsystem_personalPage_favorite.pinDocumentToFavorite({
                favId: data.favId,
                docId: data.docId,
            }).prepare().then(
                () => {
                    context.commit('documents', context.state.list.map(document => {
                        if (Number(document.id) === Number(data.docId)) {
                            document.attributes.FAVORITES = data.favId.toString();
                        }
                        return document;
                    }));
                    resolve();
                },
                () => {
                    reject();
                },
            );
        });
    },
    documentFromFavorite (context, data) {
        return new Promise((resolve, reject) => {
            services.subsystem_personalPage_favorite.unpinDocumentFromFavorite({
                favId: data.favId,
                docId: data.docId,
            }).prepare().then(
                () => {
                    context.commit('documents', context.state.list
                        .map(document => {
                            if (Number(document.id) === Number(data.docId)) {
                                document.attributes.FAVORITES = '';
                            }
                            return document;
                        })
                    );
                    resolve();
                },
                () => {
                    reject();
                },
            );
        });
    },
    clearDocuments (context) {
        context.commit('documents', []);
        context.commit('documentsTotal', 0);
    },
    documentFromFeeds (context, id) {
        context.commit('documentsDelete', id);
    },
    updateDocument (context, document) {
        context.commit('updateDocument', document);
    },
};

export default {
    state,
    actions,
    mutations
}