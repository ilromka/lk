import Vuex from 'vuex';
import state from '@/commons/store/state';
import mutations from '@/commons/store/mutations';
import actions from '@/commons/store/actions';
import modules from '@/commons/store/modules';
import channels from './channels';
import searchString from './search-string';
import tabs from './tabs';
import documents from './documents';
import languages from './languages';
import feeds from './feeds';
import favorites from './favorites';
import generalFavorites from './general-favorites';
import settings from './settings';
import users from './users';
import groups from './groups';
import recommendations from './recommendations';

export default new Vuex.Store({
    state,
    mutations,
    actions,
    modules: {
        ...modules,
        channels,
        searchString,
        tabs,
        documents,
        languages,
        feeds,
        favorites,
        generalFavorites,
        settings,
        users,
        groups,
        recommendations,
    },
});
