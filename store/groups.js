import services from '@/commons/services';

const state = {
    list: [],
};

const mutations = {
    groups (state, payload) {
        state.list = payload;
    },
};

const actions = {
    groups (context) {
        return new Promise((resolve, reject) => {
            services.securitySubjects.get({
                showGroups: true,
                showUsers: false,
                showPrivileges: false,
            }).then(
                resp => {
                    context.commit('groups', resp.data);
                    resolve();
                },
                () => {
                    reject();
                },
            );
        });
    },
};

export default {
    state,
    actions,
    mutations
}
