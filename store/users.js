import services from '@/commons/services';

const state = {
    list: [],
};

const mutations = {
    users (state, payload) {
        state.list = payload;
    },
};

const actions = {
    users (context) {
        return new Promise((resolve, reject) => {
            services.securitySubjects.get({
                showGroups: false,
                showUsers: true,
                showLockedUsers: false,
            }).then(
                resp => {
                    context.commit('users', resp.data);
                    resolve();
                },
                () => {
                    reject();
                },
            );
        });
    },
};

export default {
    state,
    actions,
    mutations
}
