import services from '@/commons/services';
import CONST from '../const';

const state = {
    offset: 0,
    total: 0,
    list: [],
    allList: [],
    currentChannel: null,
    userList: [],
};

const mutations = {
    channelsOffset (state, payload) {
        state.offset = payload;
    },
    channelsTotal (state, payload) {
        state.total = payload;
    },
    channels (state, payload) {
        state.list = payload;
    },
    channelsAppend (state, payload) {
        state.list = [...state.list, ...payload];
    },
    channel (state, payload) {
        state.currentChannel = payload;
    },
    userChannels (state, payload) {
        state.userList = payload;
    },
    subscribe (state, payload) {
        let channel = state.list.find(item => Number(item.id) === Number(payload.channelId));
        if (channel) {
            state.userList.unshift(channel);
        }
    },
    unsubscribe (state, payload) {
        state.userList = state.userList.filter(item => Number(item.id) !== Number(payload.channelId));
    },
    allChannels (state, payload) {
        state.allList = payload;
    },
    increaseChannelSubscribers (state, id) {
        state.list = state.list.map(item => {
            if (Number(item.id) === Number(id)) {
                item.subscribers = Number(item.subscribers) + 1;
            }
            return item;
        });
    },
    decreaseChannelSubscribers (state, id) {
        state.list = state.list.map(item => {
            if (Number(item.id) === Number(id)) {
                if (Number(item.subscribers) > 0) {
                    item.subscribers = Number(item.subscribers) - 1;
                }
            }
            return item;
        });
    },
};

const actions = {
    setChannelsOffset (context, value) {
        context.commit('channelsOffset', value);
    },
    setChannelsTotal (context, value) {
        context.commit('channelsTotal', value);
    },
    channels (context, name = '') {
        context.commit('channelsOffset', 0);
        return new Promise((resolve, reject) => {
            services.subsystem_personalPage_channel.list({
                offset: context.state.offset,
                count: 1000,
                name,
            }).prepare().then(
                resp => {
                    context.commit('channels', resp.data.list);
                    context.commit('channelsTotal', resp.data.total);
                    resolve();
                },
                () => {
                    reject();
                },
            );
        });
    },
    appendChannels (context, name = '') {
        context.commit('channelsOffset', context.state.offset + CONST.REQUEST.COUNT);
        return new Promise((resolve, reject) => {
            services.subsystem_personalPage_channel.list({
                offset: context.state.offset,
                count: CONST.REQUEST.COUNT,
                name,
            }).then(
                resp => {
                    context.commit('channelsAppend', resp.data.list);
                    resolve();
                },
                () => {
                    reject();
                },
            );
        });
    },
    userChannels (context, id) {
        return new Promise((resolve, reject) => {
            services.subsystem_personalPage_channel.userList(id).then(
                resp => {
                    context.commit('userChannels', resp.data);
                    resolve();
                },
                () => {
                    reject();
                },
            );
        });
    },
    setCurrentChannel (context, id) {
        let channel = context.state.userList.find(item => item.id === id);
        if (!channel) {
            channel = context.state.list.find(item => item.id === id);
        }
        if (channel) {
            context.commit('channel', channel);
        } else {
            context.commit('channel', null);
        }
    },
    subscribe (context, data) {
        return new Promise((resolve, reject) => {
            services.subsystem_personalPage_channel.userSubscribe(data.userId, data.channelId).then(
                () => {
                    context.commit('subscribe', data);
                    context.commit('increaseChannelSubscribers', data.channelId);
                    resolve();
                },
                () => {
                    reject();
                },
            );
        });
    },
    unsubscribe (context, data) {
        return new Promise((resolve, reject) => {
            services.subsystem_personalPage_channel.userUnsubscribe(data.userId, data.channelId).then(
                () => {
                    context.commit('unsubscribe', data);
                    context.commit('decreaseChannelSubscribers', data.channelId);
                    resolve();
                },
                () => {
                    reject();
                },
            );
        });
    },
    allChannels (context, name = '') {
        return new Promise((resolve, reject) => {
            services.subsystem_personalPage_channel.list({
                offset: 0,
                count: 1000,
                name,
            }).then(
                resp => {
                    context.commit('allChannels', resp.data.list);
                    resolve();
                },
                () => {
                    reject();
                },
            );
        });
    },
};

export default {
    state,
    actions,
    mutations
}
