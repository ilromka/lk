import services from '@/commons/services';

const state = {
    list: [],
    selected: [],
};

const mutations = {
    generalFavorites (state, payload) {
        state.list = payload;
    },
    selectedGeneralFavorites (state, payload) {
        state.selected = payload;
    },
};

const actions = {
    generalFavorites (context, id) {
        return new Promise((resolve, reject) => {
            services.subsystem_personalPage_generalFavorite.list({
                userId: id,
            }).then(
                resp => {
                    context.commit('generalFavorites', resp.data.list);
                    resolve();
                },
                () => {
                    reject();
                },
            );
        });
    },
    selectGeneralFavorites (context, ids) {
        context.commit('selectedGeneralFavorites', ids);
    },
    documentToGeneralFavorite (context, data) {
        return new Promise((resolve, reject) => {
            let dataObject = {
                generalFavId: data.id,
                footnote: {
                    title: data.title,
                    text: data.text,
                    author: data.author,
                    agency: data.agency,
                    url: data.url,
                }
            };
            if (data.footnoteId) {
                dataObject.footnote.id = data.footnoteId;
            }
            services.subsystem_personalPage_generalFavorite.saveFootnote(dataObject).prepare().then(
                () => {
                    resolve();
                },
                () => {
                    reject();
                },
            );
        });
    },
    documentFromGeneralFavorite (context, id) {
        return new Promise((resolve, reject) => {
            services.subsystem_personalPage_generalFavorite.deleteFootnote({
                id,
            }).prepare().then(
                () => {
                    resolve();
                },
                () => {
                    reject();
                },
            );
        });
    },
};

export default {
    state,
    actions,
    mutations
}
